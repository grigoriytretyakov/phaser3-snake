import { settings } from './settings';

class Game extends Phaser.Scene {
    constructor() {
        super({key: settings.scenes.game});
    }

    preload() {
        for (let spriteName of Object.values(settings.sprites)) {
            this.load.image(spriteName, `assets/${ spriteName }.png`);
        }
    }

    create() {
        this.foods = [];
        this.stones = [];

        this.delay = settings.delay;

        this.offsets = {
            up: {x: 0, y: -1},
            down: {x: 0, y: 1},
            left: {x: -1, y: 0},
            right: {x: 1, y: 0},
        };

        this.direction = 'right';

        const halfCell = settings.cellSize / 2;
        this.add.tileSprite(
            settings.gameWidth / 2,
            settings.gameHeight / 2,
            settings.gameWidth,
            settings.gameHeight,
            settings.sprites.ground);

        this.snake = [
            {x: 0, y: 0, name: settings.sprites.tail, direction: 'right'},
            {x: 1, y: 0, name: settings.sprites.body, direction: 'right'},
            {x: 3, y: 0, name: settings.sprites.head, direction: 'right'},
        ];
        this.snake.forEach((item, index) => {
            let [worldX, worldY] = this.cellToWorld(item.x, item.y);
            item.sprite = this.add.sprite(worldX, worldY, item.name);
        });

        while(this.stones.length < 7) {
            this.createStone();
        }

        this.input.keyboard.on('keydown', this.handleKey, this);
    }

    createStone() {
        let created = false;
        do {
            let x = Math.floor(Math.random() * settings.boardSize),
                y = Math.floor(Math.random() * settings.boardSize);

            if (!this.foods.some(f => f.x == x && f.y == y)
                && !this.stones.some(s => s.x == x && s.y == y)
                && !this.snake.some(s => s.x == x && s.y == y)) {

                created = true;

                let [worldX, worldY] = this.cellToWorld(x, y);
                this.stones.push({
                    x, y,
                    sprite: this.add.sprite(worldX, worldY, settings.sprites.stone),
                })
            }
        } while(!created);
    }

    update(time, delta) {
        this.delay -= delta;
        if (this.delay <= 0) {
            this.delay = settings.delay - this.delay;

            let first = this.snake[this.snake.length - 1];
            first.name = settings.sprites.body;
            first.sprite.setTexture(first.name);

            let x = this.convert(first.x + this.offsets[this.direction].x),
                y = this.convert(first.y + this.offsets[this.direction].y);

            if (this.snake.some(s => s.x == x && s.y == y)) {
                this.scene.restart();
                return;
            }

            if (this.stones.some(s => s.x == x && s.y == y)) {
                this.scene.restart();
                return;
            }

            let [worldX, worldY] = this.cellToWorld(x, y);

            const angles = {
                'up': -90,
                'down': 90,
                'right': 0,
                'left': 180,
            };

            let head = {
                x, y,
                name: settings.sprites.head,
                direction: this.direction,
            };
            head.sprite = this.add.sprite(worldX, worldY, head.name);
            head.sprite.angle = angles[this.direction];
            this.snake.push(head);

            let eaten = this.foods.filter(f => f.x == head.x && f.y == head.y);

            if (eaten.length > 0) {
                this.foods = this.foods.filter(f => f.x != head.x || f.y != head.y);
                eaten.forEach(f => {
                    f.sprite.destroy();
                });

                if (this.snake.length % 2) {
                    this.createStone();
                }
            }
            else {
                let drop = this.snake.shift();
                drop.sprite.destroy();

                let last = this.snake[0];
                last.name = settings.sprites.tail;
                last.sprite.setTexture(last.name);
            }
        }

        if (this.foods.length < settings.minFoodNumber) {
            this.createFoods();
        }
    }

    convert(i) {
        return (settings.boardSize + Math.sign(i) * Math.abs(i)) % settings.boardSize;
    } 

    createFoods() {
        while (this.foods.length < settings.maxFoodNumber) {
            let x = Math.floor(Math.random() * settings.boardSize),
                y = Math.floor(Math.random() * settings.boardSize);

            if (!this.foods.some(f => f.x == x && f.y == y) && !this.snake.some(s => s.x == x && s.y == y)) {
                let [foodX, foodY] = this.cellToWorld(x, y);
                this.foods.push({
                    x, y,
                    sprite: this.add.sprite(foodX, foodY, settings.sprites.food),
                })
            }
        }
    }

    reverse() {
        const inverse = {
            up: 'down',
            down: 'up',
            left: 'right',
            right: 'left',
        };

        this.snake = this.snake.reverse();
        this.snake.forEach(s => {
            s.direction = inverse[s.direction];
        });
        this.direction = this.snake[this.snake.length - 1].direction;
    }

    handleKey(e) {
        switch (e.code) {
            case 'KeyA':
            case 'ArrowLeft':
                if (this.direction == 'right') {
                    this.reverse();
                }
                else {
                    this.direction = 'left';
                }
                break;
            case 'KeyD':
            case 'ArrowRight':
                if (this.direction == 'left') {
                    this.reverse();
                }
                else {
                    this.direction = 'right';
                }
                break;
            case 'KeyW':
            case 'ArrowUp':
                if (this.direction == 'down') {
                    this.reverse();
                }
                else {
                    this.direction = 'up';
                }
                break;
            case 'KeyS':
            case 'ArrowDown':
                if (this.direction == 'up') {
                    this.reverse();
                }
                else {
                    this.direction = 'down';
                }
                break;
        }
    } 

    cellToWorld(x, y) {
        return [
            x * settings.cellSize + settings.cellSize / 2,
            y * settings.cellSize + settings.cellSize / 2
        ];
    }
}

export { Game }
