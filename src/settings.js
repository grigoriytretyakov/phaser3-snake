const boardSize = 30;

const cellSize = 16;

const gameHeight = cellSize * boardSize;

const gameWidth = cellSize * boardSize;

export const settings = {
    gameWidth,

    gameHeight,

    boardSize,
    
    cellSize,

    backgroundColor: 0x222222,

    scenes: {
        game: 'Game',
    },

    sprites: {
        head: 'head',
        body: 'body',
        tail: 'tail',
        food: 'food',
        stone: 'stone',
        ground: 'ground',
    },

    delay: 100,

    minFoodNumber: 10,
    maxFoodNumber: 50,
};

