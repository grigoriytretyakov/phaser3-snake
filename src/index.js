import 'phaser';

import { settings } from './settings';

import { Game } from './game';

function resizeGame(game) {
    let canvas = document.querySelector('canvas');
    let windowWidth = window.innerWidth;
    let windowHeight = window.innerHeight;
    let windowRatio = windowWidth / windowHeight;
    let gameRatio = game.config.width / game.config.height;
    if (windowRatio < gameRatio) {
        canvas.style.width = windowWidth + "px";
        canvas.style.height = (windowWidth / gameRatio) + "px";
    }
    else {
        canvas.style.width = (windowHeight * gameRatio) + "px";
        canvas.style.height = windowHeight + "px";
    }
}


window.onload = () => {
    document.body.style.backgroundColor = settings.backgroundColor;

    document.querySelector('h1').remove();

    const gameConfig = {
        type: Phaser.AUTO,
        width: settings.gameWidth,
        height: settings.gameHeight,
        backgroundColor: settings.backgroundColor,

        scene: [
            Game,
        ]
    };

    let game = new Phaser.Game(gameConfig);

    window.focus();
    resizeGame(game);

    window.addEventListener('resize', () => { resizeGame(game) });
}
